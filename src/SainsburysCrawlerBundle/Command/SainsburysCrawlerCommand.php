<?php
/**
 * Created by PhpStorm.
 * User: DLA13
 * Date: 22/08/2016
 * Time: 11:34
 */

namespace SainsburysCrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SainsburysCrawlerBundle\Service\SainsburysCrawlerService;

/**
 * Class SainsburysCrawlerCommand
 * @package SainsburysCrawlerBundle\Command
 */
class SainsburysCrawlerCommand extends ContainerAwareCommand
{
    /**
     * Symfony Command Configuration
     */
    protected function configure()
    {
        $this
            ->setName('sainsburys:crawl')
            ->setDescription('Crawl the provided test page and provide a JSON array of all of the products on the page')
        ;
    }

    /**
     * Symfony Command execution method. Writes the json output of get and shape to the CL
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $crawler = $this->getContainer()->get('sainsburys.crawlerservice');
        $siteData = $crawler->getSainsburysProductData();
        $shapedData = $crawler->shapeData($siteData);
        $output->write(json_encode($shapedData));
        return;
    }

}