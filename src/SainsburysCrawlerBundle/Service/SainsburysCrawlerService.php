<?php

namespace SainsburysCrawlerBundle\Service;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class SainsburysCrawlerService
 * @package SainsburysCrawlerBundle\Service
 */
class SainsburysCrawlerService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $url;

    /**
     * SainsburysCrawlerService constructor.
     * @param Client $client
     * @param $url
     */
    public function __construct(Client $client, $url)
    {
        $this->client = $client;
        $this->url = $url;
    }

    /**
     * Get data for all products on the configured URL. Loop through, and build an array of the required data for later formatting
     *
     * @return array
     */
    public function getSainsburysProductData()
    {
        $crawler = $this->client->request('GET',$this->url );
        $products = $crawler->filter('.product ')->each(function (Crawler $node, $i) {
            return [
                'title'       => trim($node->filter('.productInfo')->text()),
                'unit_price'  => $this->getPrice(trim($node->filter('.pricePerUnit')->text())),
                'description' => $this->getDescription($node->filter('.productInfo')->html()),
                'size'        => $this->getPageSize($node->filter('.productInfo')->html())
            ];
        });
        return $products;
    }

    /**
     * Shape provided data and provide totals.
     *
     * @param array $productData
     * @return array
     */
    public function shapeData(array $productData) {
        $outputData = [];
        $outputData['total'] = 0;
        foreach ($productData as $product) {
            $outputData['total']    += $product['unit_price'];
            $outputData['results'][] = $product;
        }
        $outputData['total'] = number_format($outputData['total'], 2, '.', '');
        return $outputData;
    }

    /**
     * Due to whitespacing, selectLink isn't playing ball. Due to time constraints, using regex to get link, then desc.
     *
     * @param $productHtml
     * @return string
     */
    public function getDescription($productHtml) {
        $matches = [];
        preg_match('#href=\"(.+)\">#',$productHtml,$matches);
        $crawler = $this->client->request('GET',$matches[1]);
        $descriptionNode = $crawler->filter('.productText')->first();
        return $descriptionNode->filter('p')->first()->text();
    }

    /**
     * Due to whitespacing, selectLink isn't playing ball. Due to time constraints, using regex to get link, then size
     *
     * @param $productHtml
     * @return string
     */
    public function getPageSize($productHtml) {
        $matches = [];
        preg_match('#href=\"(.+)\">#',$productHtml,$matches);
        $crawler = $this->client->request('GET',$matches[1]);
        //Could do with being more intelligent, but kb is accurate, even if measures as > Mb
        return round(strlen($crawler->html())/1024,0).'kb';
    }

    /**
     * Wrapper function to extract price from specified string
     *
     * @param $priceString
     * @return mixed
     */
    public function getPrice($priceString) {
        $matches = [];
        preg_match('#&pound(\d\.\d{2})/unit#',$priceString,$matches);
        return $matches[1];
    }
}