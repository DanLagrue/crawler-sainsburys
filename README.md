Sainsbury's Crawler Technical Test
=========

* [Info](#info)
* [Setup](#setup)
* [Run](#run)
* [Testing](#testing)
* [Tool Choices](#tools)
* [Author](#author)

## Info
This is a Symfony Command bundle, using a Service to contain all its business logic.

## Setup

Composer install (see Composer documentation: https://getcomposer.org/)

## Run

Running the following: php bin/console sainsburys:crawl will return a JSON array in the following format:

{
	"total": "15.10",
	"results": [{
		"title": "Sainsbury's Apricot Ripe & Ready x5",
		"unit_price": "3.50",
		"description": "Apricots",
		"size": "37kb"
	}...]
}

## Testing

Run phpunit. Testing is currently against live, non mocked, URL, and so is utterly dependant on web page uptime

## Tools

HTML get, CSS tag and class crawling: Goutte.

Framework: Symfony 3

Testing: PHPUnit

## Author

Dan Lagrue, 2016-08-22