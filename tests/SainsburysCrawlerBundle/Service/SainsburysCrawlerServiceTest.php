<?php
/**
 * Created by PhpStorm.
 * User: DLA13
 * Date: 22/08/2016
 * Time: 12:29
 */

namespace Tests\SainsburysCrawlerBundle\Service;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class SainsburysCrawlerServiceTest
 * @package Tests\SainsburysCrawlerBundle\Service
 */
class SainsburysCrawlerServiceTest extends WebTestCase
{
    /**
     * Sainsburys Crawler Service
     */
    private $sainsburysCrawlerService;

    private $htmlDummyData = 'href="http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html">';

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        $this->sainsburysCrawlerService = $this->getContainer()->get('sainsburys.crawlerservice');
    }

    /**
     *
     */
    public function testGetSainsburysProductData()
    {
        //Full Mock Goutte object being passed in?
        //Test against live location? <-
        //expect data array. expect array to contain 7 element and element to contain 'unit_price'#
        $productData = $this->sainsburysCrawlerService->getSainsburysProductData($this->htmlDummyData);
        $this->assertInternalType('array',$productData);
        $this->assertCount(7,$productData);
        $this->assertArrayHasKey('unit_price',$productData[0]);
    }

    /**
     *
     */
    public function testShapeData() {
        //expect data array. expect data array to contain 2 elements, results and total
        $dummyData = [
            ['unit_price' => 1],
            ['unit_price' => 2],
        ];
        $shapedData = $this->sainsburysCrawlerService->shapeData($dummyData);
        $this->assertInternalType('array',$shapedData);
        $this->assertCount(2,$shapedData);
        $this->assertArrayHasKey('results',$shapedData);
        $this->assertArrayHasKey('total',$shapedData);
    }

    /**
     *
     */
    public function testGetDescription() {
        //pass in test link
        //expect string return 'Apricots'
        $this->assertEquals('Apricots',$this->sainsburysCrawlerService->getDescription($this->htmlDummyData));
    }

    /**
     *
     */
    public function testGetPageSize() {
        //pass in test link
        //expect string return ''
        $this->assertEquals('37kb',$this->sainsburysCrawlerService->getPageSize($this->htmlDummyData));
    }

    /**
     * @param $priceString
     */
    public function testGetPrice() {
        //pass in sample html page
        //expect string return ''
        $priceString = '&pound1.50/unit';
        $this->assertEquals('1.50',$this->sainsburysCrawlerService->getPrice($priceString));
    }

}